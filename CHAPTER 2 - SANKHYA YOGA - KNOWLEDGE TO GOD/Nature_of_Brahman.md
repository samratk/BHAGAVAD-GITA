

```mermaid
graph LR;
  subgraph "Nature of Brahman."
     A("1. शाश्वत (Perpetual)")--> B("2. पुराण (Primeval)")--> C("3.अविनाशिम् (Indestructible)")--> D("4. नित्यम् (Eternal. Deathless)" )-->E("5. अजम् (Birthless)")-->F("6. अव्ययम् (Imperishable)")
  end
  subgraph "The Cosmic Cycle of birth and death."
    A1(Birth of physical and mental body.)-->B1(Death of physical body.)-->C1(Subtle & Causal body curls into seed.)-->D1(Subtle & Causal body finds another physical body)-->A1
  end
  subgraph "20 Constituents of the self."
    A2(1 - Gross Body)
    B2(2 - 19 Subtle Bodies)-->B2.1( 5 sense organs' subtle powers)
    B2-->B2.2(5 motor organs' subtle powers)
    B2-->B2.3("5 vital functions - पन्च प्राण:")
      B2.3-->B2.3.1("1. प्राण - वायु - Respiratory system")
      B2.3-->B2.3.2("2. अपान - पृथ्वि - Excretory & Reproductive system")
      B2.3-->B2.3.3("3. समान - अग्नि - Digestive system")
      B2.3-->B2.3.4("4. उदान - आकाश - Nervous system")
      B2.3-->B2.3.5("5. व्यान - जल - Coordinates all others")

    B2-->B2.4("3 - 4 Parts of the mind")
      B2.4-->B2.4.1("1. मन: (emotions, thoughts, information)")
      B2.4-->B2.4.2("2. बुद्धि: (intellect)")
      B2.4-->B2.4.3("3. चित्त: (the screen of mind)")
      B2.4-->B2.4.4("4. अह्मकार: (ego)" )
  end
```
`Podcast 6 of Swami Sarvapriyananda`
