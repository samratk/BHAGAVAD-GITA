Attachment causes desire and weaknesses. It was Arjuna's attachment with his family that was making him weak. It is the attachment, moha, that causes deep centered desires of being one with the other. That is the starting point of all evils.

The source of all this is that craving to be one. The problem is that if that craving is stop gapped with human beings in the world, or material objects in the world, that will lead to sure sort suffering. Because all objects and all human beings change. They shift. Who they are? Is there a fixed entity by that name? Sometimes they are their body. Sometimes they are their mind. Sometimes they are their desires. Sometimes they are their anger. They keep changing. Whom you call your friend? Whom you call your enemy? Everything is an ever changing flux of mind. Mind is by design an ever changing flow of energy.

Even the inanimate world is ever changing. Cracks are forming in the pillars and walls, mosses attack the plaster, the iron rusts, the shifts wreck, the ice melt, every things changes. In short term we are unable to see their changing nature. But actually everything changes.

What is unchanging in all these is God.

```mermaid
  graph TD
    A(misconception that things & people are real unchanging entities....)-->B(attachment to those unreal entities)-->C(desire)-->D("Greed, Anger, Lust, Anxiety to lose, Fear of loss")-->E(Delusion)-->F(Loss of Memory)-->G(downfall)
```
This is exactly Arjuna was going through. He was thinking his conception of the Kurus are real and unchanging. And based on that he was making reverse philosophies. And that triggered everything. That is the reason, Krishna starts to explain him the nature of the world. That the world is false. It is an ever changing phantasma. 
