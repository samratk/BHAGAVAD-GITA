<h1> CHAPTER 3 - कर्म योग: - SELFLESS ACTION </h1>


<p>
<details>
<summary> <b>1. SELFLESS ACTION WITHOUT WORLDLY DESIRES - 3.xx-3.xx </b></summary>
</details>
</p>

<p>
<details>
<summary> <b>2. DYNAMICS OF WORLDLY DESIRES 2 - 3.36-3.43 </b></summary>

![3.36](../FLASHCARDS/3.36.svg)
![3.37](../FLASHCARDS/3.37.svg)
![3.38](../FLASHCARDS/3.38.svg)
![3.39](../FLASHCARDS/3.39.svg)
![3.40](../FLASHCARDS/3.40.svg)
![3.41](../FLASHCARDS/3.41.svg)
![3.42](../FLASHCARDS/3.42.svg)
![3.43](../FLASHCARDS/3.43.svg)
</details>
</p>
