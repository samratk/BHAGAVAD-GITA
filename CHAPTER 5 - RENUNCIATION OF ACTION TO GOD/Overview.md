<h4> Idea flow </h4>
```mermaid
  graph LR
  A(KARMA YOGA)-->B(PURIFICATION)
  C(SANYASA)-->D(KNOWLEDGE OF BRAHMAN)

  B-->B1(MIND DANDA - Temple of God)-->C
  B-->B2(SPEECH DANDA - Words of God)-->C
  B-->B3(BODY DANDA - Instrument of God)-->C
```
