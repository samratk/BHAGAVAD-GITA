<h4> SESSION 1 - 3 - अनुबन्ध चतु़ष्ठय: - - 4 REMEMBERANCES - INTRODUCTION </h4>

---
1. **WHY $\rightarrow$ `प्रयोजन` $\rightarrow$ `उद्देश्य` $\rightarrow$ `पूर्णत:`**
2. **WHO $\rightarrow$ `अधिकार` $\rightarrow$ `Arjuna`**
3. **WHAT $\rightarrow$ `वि़षय वस्तु`**
4. **HOW $\rightarrow$ `सम्बन्ध`**
---

`WHY` - To attain fullness - Purnataha. It is about being totally fulfilled, in bliss, and happy. It is that feeling of total completion.

`WHO` - About the one who was eligible - Arjuna
- Arjuna did not think about his being exiled for 12 years and barged into the room when Draupadi was with Yudhisthira. He did that to save some poor men from getting the cows.
- Arjuna rejected the proposal of sex from Maneka! And did not hesitate to have suffered the curse of being an eunuch.
- Rejected the offer to marry Uttara even after seeing her beauty and have taught her dance.
- Uttar episode where he risks his life and identification by fighting the Kauravas to save Virata's kingdom.


`WHAT` - Moksha Sashtra


`HOW` -
