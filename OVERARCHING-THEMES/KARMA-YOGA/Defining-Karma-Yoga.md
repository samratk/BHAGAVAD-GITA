<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 11pt ;">

#### Transforming Karma to Karma Yoga
1. Being atma rati
2. Karma as Tat Praapti Upaya **`(तत् प्राप्ति उपाय:)`** - Yog - becomes Karma  Yoga
3. Work done with the attitude of **`नि:स्वार्थ, निर्पेक्श, निर्अहम्कार`** - Karma Yoga. Tyaga of Selfishness, Aashakti, attachment (results of action, body, mind, sense organs, me-mine-I, my family, my friends, Aham-bodha), pride, associated to actions.
4. Do your duty without Raaga, Dwesha, Kaama, Krodha.
5. Nishta to realize God. Those who are trying to climb up the mountain of God realization through their work.
6. Yagna-artha Karma - Karma Yoga - is bondage freeing. When normal work is done, following are the bondages -
   1. Results of the action
   2. Samskaaras fallout of the action.
7. `Mukta Sangha Samaachara` -
   1. Karma korte thaak
   2. Asakti tyaga korar `cheshta` chaliye jao. Karma se kuch nahin hota. Karm chalta rahega. This is the change in consciousness, This is buddy yoga. This is yoga - tat prapti upay.
   3. Samyak achara - The work that is given to you, do it with all sincerity, with the struggle of reducing the asakti.
