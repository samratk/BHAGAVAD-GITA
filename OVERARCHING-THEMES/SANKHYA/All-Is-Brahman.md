<H1>By which all is pervaded. येन सर्वम् इदम् ततम्</H1>

>**`2.16 - There is no dearth of the real. There always a dearth of the unreal.`**
**`नासतो विद्यते भावो नाभावो विद्यते सतः।`**
**`उभयोरपि दृष्टोऽन्तस्त्वनयोस्तत्त्वदर्शिभिः।।2.16।।`**

>**`2.17 - SANKHYA YOGA - RAJA YOGA`**
**`अविनाशि तु तद्विद्धि येन सर्वमिदं ततम्।`**
**`विनाशमव्ययस्यास्य न कश्चित् कर्तुमर्हति।।2.17।।`**

`Sankhya and Yoga are sister philosphies. Raja Yoga is the way of meditation`

>**`8.22 - BHAKTI YOGA`**
**`पुरुषः स परः पार्थ भक्त्या लभ्यस्त्वनन्यया।`**
**`यस्यान्तःस्थानि भूतानि येन सर्वमिदं ततम्।।8.22।।`**
`Through one pointed surrender, devotion and love to God (Bhakti) that transcendent  being can be attained, in which all being exist. And by which all is pervaded`

>**`9.4 - GNANA YOGA - ADVAIT - मयाम् तत् इदम् सर्वम् - By me all is pervaded`**
**`मया ततमिदं सर्वं जगदव्यक्तमूर्तिना।`**
**`मत्स्थानि सर्वभूतानि न चाहं तेष्ववस्थितः।।9.4।।`**

`All this is pervaded by me - The invisible all pervading.`

>**`9.5 - MAYA`**
**`न च मत्स्थानि भूतानि पश्य मे योगमैश्वरम्।`**
**`भूतभृन्न च भूतस्थो ममात्मा भूतभावनः।।9.5।।`**

`All the material world is visible only by my power. I am not in them. All are in me. In truth none of them are in me! You the pure being and pure awareness! The entire world of your experience, appears in you, shines in you, disappears in you. You pervade it all!`

>**`18.46 - KARMA YOGA -`**
**`यतः प्रवृत्तिर्भूतानां येन सर्वमिदं ततम्।`**
**`स्वकर्मणा तमभ्यर्च्य सिद्धिं विन्दति मानवः।।18.46।।`**

`Seeing Shiva in all beings. Worship them with service. शिव ज्ञाने जीव सेवा. From which has emerged all activity of the universe and all beings. By which all this is pervaded. Worshiping that one by your work, you will attain perfection.`

1. In Brahman there is no slightest space of the Samsara!
2. In the lake was there any space of mountains and trees? It was filled with water!
3. Here itself - the experiential world - is Brahman all pervading, all of these experiences in the world.
4. Why dont we experience that Brahman? **`तत् - That`**. **`इदम् - This`**. The word "this" is always used when something is immediately available for sensual perception. **`That`** - Which is remote. That is not immediately available to your senses. That is
   1. Un-perceivable - **`अद्रिश्यम्`**
   2. Not reachable by the motor organs - cannot be grasped - **`अग्राह्यम्`**

5. Abstraction and living philosophy is bridged. What is available to me is this world. Not your abstract philosophy. By that, this world of yours is pervaded through. **`ALL THIS IS PERVADED BY THAT PURE BEING`**
6. Brahman is like an incense or light that comes and pervades all that is objectified in the real world.
7. However there is no duality of that what pevades and what is illumined. In 2.16 he says one is unreal - the objectified. But that what illumines is real. The objectified borrows existence from the one that illumines it. The forest, sky, birds in the lake borrow their existence from the water. All ornaments are pervaded by the gold. All pots are pervaded by clay. There is no separate pot and clay. There is no waves and water. All names and forms are constituted through and through by the substraturm - clay or gold or water.
8. **`ठसाठस् भरपूर`**` - Pure being pervades our experienced universe in that way!
9. Of this pure being, nobody or nothing can destroy it. It does not depend on whether you fight or do not fight. All theses bodies will die. They belong to the category that had all as borrowed existence. All those you are worried about. All those you are attached to, have borrowed existence and they will go away and extinguish. Death is certain for all of them. Death of the body is certain. And your death is impossible. You cannot die. Spirit dies not. Body will die. There is nothing to be worried about. Death - is the final instrument of Maya that keeps one in terror. Body will never survive no matter how much you take care of it. And the pure self is never subject to death. It never depends on what you do.
10. By That pure being is pervaded This entire universe. Brahman does that equally. All differences are in Maya. There is no difference in Brahman. High and the low. Living and the Non-living. Male and female. Higher and lower class. In humans and animals alike - same Divinity pervades.
11. **`Verily that lowest worm is bretheren to the Nazerine`**
12. Jesus, Mahavira, Buddha, Krishna - Divinity in human form - worm is the brother to That. Yes, manifestation is different. But all these differences are in the Maya - Name and Form. The existence pervades equally to all. There is just One reality.
13. Oneness, and Sameness of all Existence! The practical implication is that inhabit this world of difference - respect, harmony, dont fight, dont murder, dont kill, dont hate! Sameness and harmony of race, gender, perspectives pervades all! All is God.
14. Divinity lies in each of us - **Equally**
15. That indestructible reality by which this samsaara is pervaded. Nobody can destroy  "This" unchangeable reality! For that Brahman he uses two words, first "That" and then "This". Pointing out thereby - that it is not in-accessible. Infact it is continually accessible! It is accessible in this way - By name and form. Seeing is the proof of existence. Proof for existence is seeing. Proof of non existence is not seeing. This rule applies only to objects. Objects are revealed by the awareness. Presence of the objects are revealed by the awareness. The absence of the objects are revealed by the awareness. **`Both the presence and absence of object reveal your awareness.`** They point to the fact that you are consciousness itself! How do you see God in all experience? This is the answer to that. In every experience God is continually revealed to us.
16. **`Reveal & Manifested!`** - The light reveals this book to you. And the book manifests the existence of light. You the consciousness, you the existence - you reveal this world. You give this world its existence. The world of appearances manifests your being - your pure being - pure awareness! Both the presence and absence of objects are revealed by your consciousness. And both the presence and absence of objects are manifested in your consciousness. So, deep sleep cannot be experienced, as mind is not working. Deep sleep does not even exist to us when we experience it. Only after deep sleep, the mind reflects the awareness saying that i had deep sleep. Deep sleep is an experience of absence. It is not abseence of consciousness. This pure being is continually available to you as your very own self, as it reveals the world. - सत्ता सफूर्ती.
17. Act with serenity in this world, without fear, anxiety, greed, attachment, lust and pure self. These bodies are with beginning and end. They are appearances of the eternal reality! Brahman. That Brahman is the embodied. The Dehi! **`स्विक्रिति मात्र`** . There is no connection between you and the body. Dehi and Deha. Sharir and Shariri.

Soundbite by Swami Sarvapriyananda of the notes above -
https://soundcloud.com/vedantany/6-bhagavad-gita-chapter-2-verse-17-18-swami-sarvapriyananda
