As a sadhak, and devotee of Krishna, you offer your work to Him at his Lotus Feet.
And whatever life has to give you take it as a Prasada.

The Prasada comes out of a deep vyakulata as well as deep peace, for God. 
