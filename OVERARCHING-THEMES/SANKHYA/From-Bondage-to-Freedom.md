<h4> Bondage and its causes </h4>

Attachment causes desire and weaknesses. It was Arjuna's attachment with his family that was making him weak. It is the attachment, moha, that causes deep centered desires of being one with the other. That is the starting point of all evils.

The source of all this is that craving to be one. The problem is that if that craving is stop gapped with human beings in the world, or material objects in the world, that will lead to sure sort suffering. Because all objects and all human beings change. They shift. Who they are? Is there a fixed entity by that name? Sometimes they are their body. Sometimes they are their mind. Sometimes they are their desires. Sometimes they are their anger. They keep changing. Whom you call your friend? Whom you call your enemy? Everything is an ever changing flux of mind. Mind is by design an ever changing flow of energy.

Even the inanimate world is ever changing. Cracks are forming in the pillars and walls, mosses attack the plaster, the iron rusts, the shifts wreck, the ice melt, every things changes. In short term we are unable to see their changing nature. But actually everything changes.

What is unchanging in all these is God.

```mermaid
  graph TD
    A(misconception that things & people are real unchanging entities....)-->B(attachment to those unreal entities)-->C(desire)-->D("Greed, Anger, Lust, Anxiety to lose, Fear of loss")-->E(Delusion)-->F(Loss of Memory)-->G(downfall)
```
This is exactly Arjuna was going through. He was thinking his conception of the Kurus are real and unchanging. And based on that he was making reverse philosophies. And that triggered everything. That is the reason, Krishna starts to explain him the nature of the world. That the world is false. It is an ever changing phantasma.

<h4> The joy of the freedom </h4>
When you are really Asangha. When you are really not attached to this unreal world, you do not lose your peace of mind. You know the people who are loving you now, will hate you later, and the people who are hating you now might either die way later or start loving you later. That deep awareness that the people always operate in the level of the mind, and mind is always selfish. So, for selfish motive people will take up any form anytime. They are frog now, they will become tortoise tomorrow. In long term that is what even happened in the bodily level. Fishes became the amphibians and then reptiles and then mammals and then humans. The life forms keep changing. When you really have that ingrained in your psyche deeply that people are showing up as a movie screen, and that you need not to take them at face value at all. And when you always keep in mind this experience of death, this experience of betrayal, this experience of changing opinions, the erasing away of memory, all these will remind you of the flimsiness of the world.
And when that is ingrained, you become like lotus leaf, and you don't get soiled by the tantrums of the people.

<h4> The relative levels of truth
```mermaid
  graph TD
  A(God)-->B(Outcome non-human objects)-->C(concrete non human results and real events)-->D(human bodies)-->E(human actions)-->F(human words)-->G(human thoughts & opinions)
```

<h4> The freedom experience </h4>
When that deep seated dis-interest - Uparati - is developed. When that Vairagya is developed, one is not impacted with the dealings of the world, you really do not care of the human words and actions and opinions. What you are focussed on is the objective truth. And beyond that objective truth, you focus on God! Thats all.

<h4> How to escape the hatred </h4> 
