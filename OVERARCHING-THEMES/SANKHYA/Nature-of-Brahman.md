<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

<h3> Nature of Brahman </h3>
```mermaid
graph LR;
  A("नित्यम्" )-->B("अव्ययम्") --> C("अजम्") --> D("शाश्वत") --> E("पुराण")
```

1. The world is an appearance, illumined by the wtiness - the is-ness - the Self - the Brahman.
2. There is only one - Is - Brahman. Everything else is `Is-Not`
3. Brahman did not create anything. The actual cause of thing, that line of causation is within the relative frame of reference of time, space and causation itself. That is the definition of Maya. So, you cannot say what is the cause of the Maya, as the cause of a thing is a thing itself, and hence part of Maya.
4. The very nature of Brahman is - Existence, Consciousness and Bliss. That is what defines Brahman. It is like Sun has fire in it. That is the very nature of Sun. Similarly, the very nature of Brahman is Happiness, Consciousness and Existence. Note that Sat Chit Ananda are not in Brahman. Brahman itself is Sat Chit Ananda. It is like water is not in the wave. Wave itself is water. It is not that fire is in a thing called Sun. Sun itself is fire.
5. In this world you have three things -
   1. Sat - That is. Brahman
   2. Asat - That is not. Like a circle square
   3. Mithya - That is not. But appears as is. Maya. The Upadhi with which Brahman shows up.
6. Brahman - The Sat Chit Ananda - is a subject which reveals the objective world. The objective world is manifested by this revelation of the Brahman. When we say Aham Brahmasmi, we do not say that we are that thing God. Because Brahman is not an object. By saying Aham Brahmasmi, we mean that I am that witness consciousness that is revealing everything - including me (my mind, ego, intellect).
7. There is a sense of freedom and a freshness and lightness when I relate to that witness consciousness deep in my meditation. Dwait idea of relating Krishna as a person, seems stifling. It appears limited by updadhi - in this case. However when you focus on and are aware of that witness consciousness that is illumining and revealing everything including me, that makes you go outside of your little mind, and the separateness between minds and people. 
